<?php

class Helena {
	public function __construct() {
		$this->get = $_GET;
		
		self::visitorLog();
	}
	
	/*
	 * Bool.
	 */
	private function isClientExist() {
		return (!isset($this->get['client']) || is_null($this->get['client'])) ? FALSE : is_dir(self::clientFile("cli_dir"));
	}
	
	/*
	 * -2		jika client belum di set
	 * -1		jika client tidak ada
	 * NULL		jika tidak ada
	 * String 	jika berhasil
	 */
	private function clientFile($key) {
		if(!isset($this->get['client']))
			return -2;
		if(self::isClientExist() == FALSE)
			return -1;
		
		$this->cli_dir_f				= "./client/" . $this->get['client'];
		$this->req_log_f				= $this->cli_dir_f . "/req_log.txt";
		$this->way_inf_f				= $this->cli_dir_f . "/way.txt";
		$this->srv_stt_f				= $this->cli_dir_f . "/srv_stat.txt";
		$this->cli_bln_f				= $this->cli_dir_f . "/cli_bl.txt";
		
		switch($key) {
			case "cli_dir"		: $ret	= $this->cli_dir_f;	break;
			case "req_log"		: $ret	= $this->req_log_f;	break;
			case "cli_get"		: $ret	= $this->cli_inf_f;	break;
			case "way_inf"		: $ret	= $this->way_inf_f;	break;
			case "srv_stt"		: $ret	= $this->srv_stt_f;	break;
			case "cli_bln"		: $ret	= $this->cli_bln_f;	break;
			default				: $ret	= NULL;				break;
		}
		
		return $ret;
	}
	
	/*
	 * Tanggal
	 */
	private function dateNow() {
		return date('d M Y - H:i');
	}
	
	/*
	 * -3	jika gagal membuat direktori
	 * -2	jika client belum di set
	 * -1	jika client sudah terdaftar
	 * 0	jika OK
	 */
	private function createClient() {
		if(!isset($this->get['client']))
			return -2;
		if(!is_dir("./client"))
			if(mkdir("./client/" . $this->get['client'], 0755) == FALSE)
				return -3;
		if(self::isClientExist() == TRUE)
			return -1;
		if(mkdir("./client/" . $this->get['client'], 0755) == FALSE)
			return -3;
		
		$file_key	= array("req_log", "cli_get", "way_inf", "srv_stt");
		$i			= 0;
		
		while($i<count($file_key)) {
			if(!is_null($f=self::clientFile($file_key[$i])))
				if(touch($f) == FALSE)
					return $i;
			
			$i++;
		}
		
		file_put_contents(self::clientFile("way_inf"), "127.0.0.1:2222");
		file_put_contents(self::clientFile("srv_stt"), "0");
		
		return 0;
	}
	
	/*
	 * -5	jika client belum di set
	 * -4	jika client belum di set
	 * -3	jika client tidak ada
	 * -2	jika client belum di set
	 * -1	jika client tidak ada
	 * 0	jika OK
	 * 1	jika gagal menulis data ke file way_inf
	 * 2	jika gagal menulis data ke srv_stt
	 */
	private function setDefaultClientFileVal() {
		if(($srvstt_file=self::clientFile("srv_stt")) == -1)
			return -1;
		if($srvstt_file == -2)
			return -2;
		if(($wayinf_file=self::clientFile("way_inf")) == -1)
			return -3;
		if($wayinf_file == -2)
			return -4;
		if(!isset($this->get['client']))
			return -5;
		if(file_put_contents($wayinf_file, "127.0.0.1:2222" . PHP_EOL) == FALSE)
			return 1;
		if(file_put_contents($srvstt_file, "0" . PHP_EOL) == FALSE)
			return 2;
		
		return 0;
	}
	
	/*
	 * -2	jika client belum di set
	 * -1	jika client tidak ada
	 * 0	jika OK
	 * 1	jika ada kesalahan menulis data ke req_log
	 */
	private function saveClientReqToLog() {
		if(($cli_file=self::clientFile("req_log")) == -1)
			return -1;
		if($cli_file == -2)
			return -2;
		if(file_put_contents($cli_file, "[" . self::dateNow() . "] " . $_SERVER['REMOTE_ADDR'] . PHP_EOL, FILE_APPEND) == FALSE)
			return 1;
		
		return 0;
	}
	
	/*
	 * -3		jika gagal membaca data
	 * -2		jika client belum di set
	 * -1		jika client tidak ada
	 * NULL		jika way_inf == NULL
	 * String	jika berhasil
	 */
	private function getWayInfo() {
		if(($cli_file=self::clientFile("way_inf")) == -1)
			return -1;
		if($cli_file == -2)
			return -2;
		if(($way=file_get_contents($cli_file)) === FALSE)
			return -3;
		
		return $way;
	}
	
	/*
	 * -3		jika gagal membaca data
	 * -2		jika client belum di set
	 * -1		jika client tidak ada
	 * NULL		jika srv_stt == NULL
	 * String	jika berhasil
	 */
	private function getSrvStat() {
		if(($cli_file=self::clientFile("srv_stt")) == -1)
			return -1;
		if($cli_file == -2)
			return -2;
		if(($stat=file_get_contents($cli_file)) === FALSE)
			return -3;
		
		return $stat;
	}
	
	/*
	 * -5		jika gagal menulis ke file srv_stt
	 * -4		jika val tidak valid
	 * -3		jika val == NULL
	 * -2		jika client belum di set
	 * -1		jika client tidak ada
	 * 0		jika OK
	 */
	private function setServerStatus() {
		if(($cli_file=self::clientFile("srv_stt")) == -1)
			return -1;
		if($cli_file == -2)
			return -2;
		if(!isset($this->get['val']))
			return -3;
		if(($this->get['val'] != 0) && ($this->get['val'] != 1))
			return -4;
		if(file_put_contents($cli_file, $this->get['val'] . PHP_EOL) == FALSE)
			return -5;
		
		return 0;
	}
	
	/*
	 * -4		jika gagal menulis ke file way_inf
	 * -3		jika val == NULL
	 * -2		jika client belum di set
	 * -1		jika client tidak ada
	 * 0		jika OK
	 */
	private function setWayInfo() {
		if(($cli_file=self::clientFile("way_inf")) == -1)
			return -1;
		if($cli_file == -2)
			return -2;
		if(!isset($this->get['val']))
			return -3;
		if(file_put_contents($cli_file, $this->get['val'] . PHP_EOL) == FALSE)
			return -4;
		
		return 0;
	}
	
	/*
	 * -1		jika gagal scan direktori
	 * String	jika OK
	 */
	private function clientList() {
		if(($dir_list=scandir("client")) == FALSE)
			return -1;
		
		$ret = array();
		
		foreach($dir_list as $val) {
			if(($val == ".") || ($val == ".."))
				continue;
			if(!is_dir("client/" . $val))
				continue;
			if($ret == NULL)
				$ret = $val;
			
			$ret = $ret . "\n" . $val;
		}
		
		return $ret;
	}
	
	/*
	 * 
	 * -3	jika gagal membuat direktori
	 * -2	jika client belum di set
	 * -1	jika client sudah terdaftar
	 * 0	jika OK
	 * >0	jika gagal membuat/menulis file
	 */
	private function tell() {
		if(!isset($this->get['client']))
			return -2;
		
		if( (!isset($this->get['build_number'])) || (!is_numeric($this->get['build_number'])) || (is_null($this->get['build_number'])) )
			$build_number = 0;
		else
			$build_number = $this->get['build_number'];
		
		if(self::isClientExist() == FALSE)
			if(($mkcli=self::createClient()) != 0)
				return $mkcli;
		
		if(file_put_contents(self::clientFile("req_log"), "[ " . self::dateNow() . " ] " . $_SERVER['REMOTE_ADDR'] . PHP_EOL, FILE_APPEND) == FALSE)
			return 1;
		if(file_put_contents(self::clientFile("cli_bln"), "[ " . self::dateNow() . " ]" . $build_number  . PHP_EOL, FILE_APPEND) == FALSE)
			return 2;
		
		return 0;
	}
	
	/*
	 * Hapus rekrusif
	 */
	private function rrm($fod) {
		if(is_dir($fod)) {
			$objects = scandir($fod);
			foreach($objects as $object) {
				if($object != "." && $object != "..")
					if(is_dir($fod."/".$object))
						rrm($fod."/".$object);
					else
						unlink($fod."/".$object);
			}
			rmdir($fod);
		}
		else
			unlink($fod);
		
		return TRUE;
	}
	
	/*
	 * -1	jika client tidak ada aatu belum di set
	 * 0	jika OK
	 */
	private function rmClient() {
		if(self::isClientExist() == FALSE)
			return -1;
		
		self::rrm("client/".$this->get['client']);
		
		return 0;
	}
	
	/*
	 * -1	jika gagal membuat direktori update
	 * -2	jika gagal membuat file update/lates.version
	 * -3	jika gagal mengisi file update/lates.version
	 * -4	jika gagal membaca file update/lates.version
	 * >0	jika OK
	 */
	private function getLastClientBuildNumber() {	
		if(!is_dir("./update"))
			if(!mkdir("./update", 0755))
				return -1;
		
		$ret = NULL;
		
		if(!is_file("./update/lates.build"))
			if(!touch("./update/lates.build"))
				return -2;
			else
				if(file_put_contents("./update/lates.build", "0" . PHP_EOL) == FALSE)
					return -3;
				else
					$ret = "-1";
		else
			if(($ret = file_get_contents("./update/lates.build")) === FALSE)
				return -4;
		
		return (int)$ret;
	}
	
	/*
	 * -1	jika val belum di set
	 * -2	jika gagal mendapatkan versi terakhir
	 * -3	jika versi lebih kecil dari versi terakhir
	 * -4	jika gagal menulis ke file
	 * >=0	jika OK
	 */
	private function setLastClientBuildNumber() {
		if(($lates_ver = self::getLastClientBuildNumber()) < 0)
			return -1;
		if((int)$this->get['val'] <= $lates_ver)
			return -3;
		if(file_put_contents("update/lates.build", $this->get['val'] . PHP_EOL) == FALSE)
			return -4;
		
		return 0;
	}
	
	private function pushLog() {
		return file_put_contents("update/push.log", "[" . $this->dateNow() . "] " . $this->get['val'] . " " . $_SERVER['REMOTE_ADDR'] . PHP_EOL, FILE_APPEND);
	}
	
	/*
	 * -7	jika gagal memindah file
	 * -6	jika upload terlalu besar, maksimal 500000
	 * -5	jika file bukan gzip
	 * -4	jika upload error
	 * -3	jika versi tidak lebih besar dari versi terakhir
	 * -2	jika gagal membuat direktori update
	 * -1	jika val belum di set
	 * 0	jika OK
	 */
	private function pushClientCli() {
		if(!isset($this->get['val']))
			return -1;
		
		if(!is_dir("update"))
			if(!mkdir("update", 0755))
				return -2;
		
		if(($lates=self::getLastClientBuildNumber()) != -1)
			if(($val=(int)$this->get['val']) <= $lates)
				return -3;
		
		// Check for errors
		if($_FILES['file_upload']['error'] > 0)
			return -4;

		/*
		if(!getimagesize($_FILES['file_upload']['tmp_name'])){
			die('Please ensure you are uploading an image.\n');
		}
		*/

		/*
		// Check filetype
		if($_FILES['file_upload']['type'] != 'image/png'){
			die('Unsupported filetype uploaded.\n');
		}
		*/
		
		/*
		// Check filetype
		if($_FILES['file_upload']['type'] != 'application/x-compressed')
			return -5;
		*/
		
		// Check filetype
		if($_FILES['file_upload']['type'] != 'application/octet-stream')
			return -5;
		
		// Check filesize
		if($_FILES['file_upload']['size'] > 500000)
			return -6;
		
		/*
		// Check if the file exists
		if(file_exists('uploaded/' . $_FILES['file_upload']['name']))
			return 0
		*/
		
		// Upload file
		if(!move_uploaded_file($_FILES['file_upload']['tmp_name'], 'update/helena-' . $this->get['val'] . ".tar.gz"))
			return -7;
		
		self::setLastClientBuildNumber();
		self::pushLog();
		

		return 0;
	}
	
	private function downloadLastClient() {
		if(($last = self::getLastClientBuildNumber()) < 0)
			return -1;
		
		
		$file_name	= "helena-" . $last . ".tar.gz";
		$file_path	= "update/" . $file_name;
		
		if(file_exists($file_path) != true)
			return -2;
		
		header('Content-type: application/x-compressed');
		header('Content-Disposition: attachment; filename="' . $file_name . '"');
		header('Content-Transfer-Encoding: binary');
		
		readfile($file_path);
		
		die();
	}
	
	private function getLastClientLink() {
		if(($last=self::getLastClientBuildNumber()) < 0)
			return -1;
		
		$file_name	= "helena-" . $last . ".tar.gz";
		$file_path	= "update/" . $file_name;
		
		if(is_file($file_path) == FALSE)
			return -2;
		
		return $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER['PHP_SELF']) . "/" . $file_path;
	}
	
	/*
	 * IP addr
	 */
	private function whatsMyIp() {
		return $_SERVER['REMOTE_ADDR'];
	}
	
	/*
	 * User agent
	 */
	private function whatsMyUa() {
		return $_SERVER['HTTP_USER_AGENT'];
	}
	
	private function visitorLog() {
		$log_file	= "visitor.log";
		$separator	= "=====================================================================================================";
		$arr_key	= array_keys($_SERVER);
		
		$str		= $separator . "\n";
		$str		= $str . self::dateNow() . "\n";
		$str		= $str . $separator . "\n";
		
		for($i=0; $i<sizeof($arr_key); $i++) {
			$str = $str . $arr_key[$i] . " = " . $_SERVER[$arr_key[$i]] . "\n";
		}
		
		$str	= $str . $separator . "\n";
		
		if(file_put_contents($log_file, $str . PHP_EOL, FILE_APPEND) == FALSE)
			return 1;
		
		return 0;
	}
	
	/*
	 * Ewweewwwwww
	 */
	private function helpMe() {
		echo "\$_GET var list	: \n";
		echo "		cmd			: Printah.\n";
		echo "		client			: Nama klien. Lihat tuntunan perintah apakah ini di minta atau tidak.\n";
		echo "		val			: Nilai yang di berikan. Lihat tuntunan perintah apakah ini di minta atau tidak.\n";
		echo "\n";
		echo "cmd list	:\n";
		echo "		is_client_exist		: Apakah klien ada? Membutuhkan variabel 'client'.\n";
		echo "		save_client_req		: Simpan log rekues. Membutuhkan variabel 'client'.\n";
		echo "		get_server_status	: Dapatkan status_server dari klien yang di maksud. Membutuhkan variabel 'client'\n";
		echo "		set_server_status	: Set server_status dari klien yang di maksud. Membutuhkan variabel 'client' dan 'val'.\n";
		echo "		get_way_info		: Dapatkan way_info. Membutuhkan variabel 'cleint'\n";
		echo "		set_way_info		: Set way_info dari klien yang di maksud. Membutuhkan variabel 'client' dan 'val'.\n";
		echo "		tell			: Simpan data yang di berikan. Membutuhkan variabel 'client' dan 'build_number'.\n";
		echo "		list_client		: Melihat client yang terdaftar.\n";
		echo "		rm_client		: Menghapus client. Membutuhkan variabel 'client'.\n";
		echo "		push_client_cli		: Upload client versi baru. Membutuhkan variabel 'val'. Form method 'post', form enctype 'multipart/form-data'. Input type 'file', input name 'file_upload'\n";
		echo "		get_last_client_bn	: Dapatkan build number binari klien yang terakhir ter-upload.\n";
		echo "		download		: Dapatkan binari klien terbaru.\n";
		echo "		download_last_client	: Alias dari perintah download.\n";
		echo "		get_last_client_link	: Dapatkan link binari klien terbaru.\n";
		echo "		whats_my_ip		: Dapatkan IP.\n";
		echo "		whats_my_ua		: Dapatkan user agent.\n";
		echo "\n";
		echo "val format	:\n";
		echo "		set_server_status	: 0 untuk noaktif dan 1 untuk aktif.\n";
		echo "		set_way_info		: Host dan port server. Contoh : 192.168.0.1:222\n";
		echo "		push_client_cli		: Versi client. Versi bertipe integer di mulai dari 0\n";
		echo "		tell			: 'client' adalah nama client dan 'build_number' adalah build number client.\n";
	}
	
	private function sel() {
		if(!isset($this->get['cmd']))
			return "Cmd == NULL.";
		
		switch($this->get['cmd']) {
			case "is_client_exist"			:
												switch($act=self::isClientExist()) {
													case TRUE	:
																	$ret = "TRUE";
																break;
													case FALSE	:
																	$ret = "FALSE";
																break;
													default		:
																	$ret = "Unknown error.";
																break;
												}
											break;
			case "save_client_get"			:
												switch($act=self::saveCliGetToLog()) {
													case -2	:	$ret = "Client is not set.";
															break;
													case -1	:	$ret = "Client is not exist.";
															break;
													case 0	:	$ret = "OK";
															break;
													default	:	$ret = "Failed to write data.";
															break;
												}
											break;
			case "get_way_info"				:
												switch($act=self::getWayInfo()) {
													case -3		:	$ret = "Failed to get data from the log file.";
																break;
													case -2		:	$ret = "Client is not set.";
																break;
													case -1		:	$ret = "Client not exist.";
																break;
													case NULL	:	$ret = "NULL";
																break;
													default		:	$ret = $act;
																break;
												}
											break;
			case "get_server_status"		:
												switch($act=self::getSrvStat()) {
													case -3		:	$ret = "Failed to get data from the log file.";
																break;
													case -2		:	$ret = "Client is not set.";
																break;
													case -1		:	$ret = "Client not exist.";
																break;
													case NULL	:	$ret = "NULL";
																break;
													default		:	$ret = $act;
																break;
												}
											break;
			case "set_server_status"		:
												switch($act=self::setServerStatus()) {
													case -5		:	$ret = "Failed to save data to the log file.";
																break;
													case -4		:	$ret = "Invalid value. 0 is inactive, 1 is active.";
																break;
													case -3		:	$ret = '$_GET[val] is NULL';
																break;
													case -2		:	$ret = "Client is not set.";
																break;
													case -1		:	$ret = "Client not exist.";
																break;
													case 0		:	$ret = "TRUE";
																break;
													default		:	$ret = "Unlnown error.";
																break;
												}
											break;
			case "set_way_info"				:
												switch($act=self::setWayInfo()) {
													case -4		:	$ret = "Failed to save data to the log file.";
																break;
													case -3		:	$ret = "Val is NULL.";
																break;
													case -2		:	$ret = "Client is not set.";
																break;
													case -1		:	$ret = "Client is not exist.";
																break;
													case 0		:	$ret = "OK";
																break;
													default		:	$ret = "Unknown error.";
																break;
												}
											break;
			case "list_client"				:
												switch($act=self::clientList()) {
													case -1		:	$ret = "Scandir failed.";
																break;
													default		:	if(sizeof($act) == 0)
																		$ret = "NULL";
																	else
																		$ret = $act;
																break;
												}
											break;
			case "tell"						:
												switch($act=self::tell()) {
													case -3		:	$ret = "Create dir failed.";
																break;
													case -2		:	$ret = "Client is not set.";
																break;
													case -1		:	$ret = "Client is already exist.";
																break;
													case 0		:	$ret = "OK";
																break;
													default		:	$ret = "Create/write file failed.";
																break;
												}
											break;
			case "rm_client"				:
												switch($act=self::rmClient()) {
													case -1		:	$ret = "Client is not exist.";
																break;
													default		:	$ret = "OK";
																break;
												}
											break;
			case "push_client"				:
												switch($act=self::pushClientCli()) {
													case -7		:	$ret = "Failed to move uploaded file.";
																break;
													case -6		:	$ret = "Maximum upload is 500000.";
																break;
													case -5		:	$ret = "It's not gzip file.";
																break;
													case -4		:	$ret = "Upload error.";
																break;
													case -3		:	$ret = "Push version is <= from lates version.";
																break;
													case -2		:	$ret = "Filed to make directory update.";
																break;
													case -1		:	$ret = "Val is not set.";
																break;
													case 0		:	$ret = "OK";
																break;
													default		:	$ret = "Unknown error.";
																break;
												}
											break;
			case "get_last_client_bn"	:
												switch($act=self::getLastClientBuildNumber()) {
													case -4		:	$ret = "Failed to read data.";
																break;
													case -3		:	$ret = "Failed to write data.";
																break;
													case -2		:	$ret = "Failed to create file.";
																break;
													case -1		:	$ret = "Failed to create directory.";
																break;
													default		:	$ret = $act;
																break;
												}
											break;
			case "download"					:
			case "download_last_client"		:
												switch($act=self::downloadLastClient()) {
													case -2		:	$ret = "File not found.";
																break;
													case -1		:	$ret = "Get last client version failed.";
																break;
													default		:	$ret = "OK";
																break;
												}
											break;
			case "get_last_client_link"		:
												switch($act=self::getLastClientLink()) {
													case -2		:	$ret = "File not found.";
																break;
													case -1		:	$ret = "Get last client version failed.";
																break;
													default		:	$ret = $act;
																break;
												}
											break;
			case "help"						:
												$ret = self::helpMe();
											break;
			case "whats_my_ip"				:
												$ret = self::whatsMyIp();
											break;
			case "whats_my_ua"				:
												$ret = self::whatsMyUa();
											break;
			default							:
												$ret = "Use '\$_GET['cmd'] = help' for help";
											break;
		}
		
		return $ret;
	}
	
	/*
	 * Fire!!!!
	 */
	public function fire() {
		echo self::sel();
		
		return TRUE;
	}
}
